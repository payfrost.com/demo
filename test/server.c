#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#ifndef __PSEC_H__
#define __PSEC_H__
#include "../headers/psec.h"
#endif

int main(void)
{

	unsigned short port = 80;

	int sockfd = 0, new_sockfd = 0;

	struct sockaddr_in host_addr,client_addr;

	memset_s(&host_addr,sizeof(struct sockaddr_in),0x00,sizeof(struct sockaddr_in));
	
	memset_s(&client_addr,sizeof(struct sockaddr_in),0x00,sizeof(struct sockaddr_in));
	

	socklen_t sin_size;

	memset_s(&sin_size,sizeof(socklen_t),0x00,sizeof(socklen_t));

	size_t recv_length = 0, yes = 1;

	unsigned char buf[1025];

	memset_s(buf,1025*sizeof(unsigned char),0x00,1025*sizeof(unsigned char));

	if ( ( sockfd = socket(PF_INET,SOCK_STREAM,0)) == -1 )
	{
		perror("in socket");
	}


if ( setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1 ) { perror("setting socket option SO_REUSEADDR"); }
	host_addr.sin_family = AF_INET; // Host byte order

	if ( port < 0 )
	{
		fprintf(stderr,"Error: port number is negative.\nAborting.\n");

		exit(0);
	}

	host_addr.sin_port = htons(port); // Short, network byte order

	host_addr.sin_addr.s_addr = 0; // Automatically fill with my IP address.

	memset_s(&(host_addr.sin_zero), 8,0x00,8); // Zero the rest of the struct

	if ( bind(sockfd,(struct sockaddr*)&host_addr,sizeof(struct sockaddr)) == -1 )
	{
		perror("binding to socket failure");
	}

	if ( listen(sockfd,5) == -1 )
	{
		perror("listening to socket failure");
	}
	
	unsigned char ipv4[17];

	memset_s(ipv4,17*sizeof(unsigned char),0x00,17*sizeof(unsigned char));


	while (1)
	{
		sin_size = sizeof(struct sockaddr_in);

		new_sockfd = accept(sockfd,(struct sockaddr *)&client_addr,&sin_size);

		if ( new_sockfd == -1 )
		{
			perror("accept()ing conection failure");
		}

//		inet_ntop(AF_INET,client_addr.sin_addr,ipv4
		

		printf("server: got connection from %s port %d\n",inet_ntop(AF_INET,&(client_addr),ipv4,INET_ADDRSTRLEN),ntohs(client_addr.sin_port));


		send(new_sockfd,"Hello, World!\n",14,0);

		recv_length = recv(new_sockfd,&buf,1024,0);

		while ( recv_length > 0 )
		{
			printf("RECV: %zu bytes\n",recv_length);

			printf("recv:%s\n",buf);

			recv_length = recv(new_sockfd,&buf,1024,0);
		}

		close(new_sockfd);
	}


	return 0;
}
