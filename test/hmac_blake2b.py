import pysodium
import base64

# Takes in string argument and key string and outputs base64 encoded string

def hmac_message(key,arg):

   hmac = pysodium.crypto_generichash_blake2b_salt_personal(arg,outlen = pysodium.crypto_generichash_blake2b_BYTES,key = key,salt=b'0000000000000000',personal=b'0000000000000000')

#   print(type(hmac))

#   print(hmac)

#b64encode takes in 'bytes' and outputs 'bytes'

   base64_hmac = base64.b64encode(hmac).decode()

#   print(type(base64_hmac))

#   print(base64_hmac)

hmac_message("Message","this is super safe secret")
