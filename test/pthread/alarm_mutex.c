#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

typedef struct alarm_tag
{
	struct alarm_tag * link;

	int    seconds;

	time_t time;

	unsigned char message[65];
} alarm_t;

pthread_mutex_t alarm_mutex = PTHREAD_MUTEX_INITIALIZER;

alarm_t * alarm_list = NULL;

void * alarm_thread(void * arg)
{
	alarm_t * alarm;

	int sleep_time;

	time_t now;

	int status;

	while ( 1 )
	{
		status = pthread_mutex_lock(&alarm_mutex);

		if ( status != 0 )
		{
			fprintf(stderr,"Error: Failed to lock mutex in alarm_thread().\nAborting.\n");

			exit(1);
		}

		alarm = alarm_list;

		if ( alarm == NULL )
		{
			sleep_time = 1;
		}

		else
		{
			alarm_list = alarm->link;

			now = time(NULL);

			if ( alarm->time <= now )
			{
				sleep_time = 0;
			}

			else
			{
				sleep_time = alarm->time - now;
			}
		}

#ifdef DEBUG
		printf("[waiting: %d(%d)\"%s\"]\n",alarm->time,sleep_time,alarm->message);
#endif
		status = pthread_mutex_unlock(&alarm_mutex);

		if ( status != 0 )
		{
			fprintf(stderr,"Error: Unlock mutex in alarm_thread() failed.\nAborting.\n");

			exit(1);
		}

		if ( sleep_time > 0 )
		{
			sleep(sleep_time);
		}

		else
		{
			sched_yield();
		}

		if ( alarm != NULL )
		{
			printf("(%d) %s\n",alarm->seconds,alarm->message);

			free(alarm);
		}
	}
}

int main(int argc,char * argv[])
{
	int status;

	unsigned char line[128];

	alarm_t * alarm, ** last, *next;

	pthread_t thread;

	status = pthread_create(&thread,NULL,alarm_thread,NULL);

	if ( status != 0 )
	{
		fprintf(stderr,"Error: Failed to create alarm thread in main().\nAborting.\n");

		exit(1);
	}

	while ( 1 )
	{
		printf("alarm> ");

		if ( fgets(line,sizeof(line),stdin) == NULL ) exit(0);

		if ( strnlen(line,1024) <= 1 ) continue;

		alarm = (alarm_t*)malloc(sizeof(alarm_t));

		if ( alarm == NULL )
		{
			fprintf(stderr,"Error: Dynamically allocate alarm failed in main().\nAborting.\n");

			exit(1);
		}

		if ( sscanf(line,"%d %64[^\n]",&alarm->seconds,alarm->message) < 2 )
		{
			fprintf(stderr,"Bad command\n");

			free(alarm);
		}

		else
		{
			status = pthread_mutex_lock(&alarm_mutex);

			if ( status != 0 )
			{
				fprintf(stderr,"Error: Failed to lock mutex in main().\nAborting.\n");

				exit(1);
			}

			alarm->time = time(NULL);

			last = &alarm_list;

			next = *last;

			while ( next != NULL )
			{
				if ( next->time >= alarm->time )
				{
					alarm->link = next;

					*last = alarm;

					break;
				}

				last = &next->link;

				next = next->link;
			}

			if ( next == NULL )
			{
				*last = alarm;

				alarm->link = NULL;
			}

#ifdef DEBUG
			printf("[list: ");

			for ( next = alarm_list; next != NULL; next = next->link )
			{
				printf("%d(%d)[\"%s\"] ",next->time,next->time - time(NULL),next->message);

			}

			printf("[\n");
#endif
			status = pthread_mutex_unlock(&alarm_mutex);

			if ( status != 0 )
			{
				fprintf(stderr,"Error: Failed to unlock mutex in main().\nAborting.\n");

				exit(1);
			}
		}
	}
}
