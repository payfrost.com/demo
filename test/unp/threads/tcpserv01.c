#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/un.h>
#include <pthread.h>

void str_echo(int sockfd)
{
	ssize_t n;

	unsigned char buf[1025];

	again:

		while ( ( n = read(sockfd,buf,1024) ) > 0 )
		{
			write(sockfd,buf,n);
		}

		if ( ( n < 0 ) && errno == EINTR )
		{
			goto again;
		}

		else if ( n < 0 )
		{
			fprintf(stderr,"Error: Failed to receive input from user.\nAborting.\n");

			exit(0);
		}

}

int tcp_listen(const unsigned char * host, const unsigned char * serv,socklen_t * addrlenp)
{
	int listenfd = 0, n = 0;

	const int on = 1;

	struct addrinfo hints, * res, * ressave;

	memset(&hints,0x00,sizeof(struct addrinfo));

	hints.ai_flags = AI_PASSIVE;

	hints.ai_family = AF_UNSPEC;

	hints.ai_socktype = SOCK_STREAM;

	if ( ( n = getaddrinfo(host,serv,&hints,&res) ) != 0 )
	{
		fprintf(stderr,"tcp_listen error for %s, %s: %s\n",host,serv,gai_strerror(n));
		
		exit(1);
	}

	ressave = res;

	do
	{
		listenfd = socket(res->ai_family,res->ai_socktype,res->ai_protocol);

		if ( listenfd < 0 )
		{
			continue;
		}

		setsockopt(listenfd,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));

		if ( bind(listenfd,res->ai_addr,res->ai_addrlen) == 0 )
		{
			break;
		}

		close(listenfd);

	} while( ( res = res->ai_next ) != NULL );

	if ( res == NULL )
	{
		fprintf(stderr,"Error: tcp_listen error for %s, %s\n",host,serv);

		exit(1);
	}

	listen(listenfd,1024);

	if ( addrlenp != NULL )
	{
		*addrlenp = res->ai_addrlen;

	}

	freeaddrinfo(ressave);

	return listenfd;
}

static void * doit(void * arg)
{
	int connfd;

	connfd = *((int*)arg);

	free(arg);

	pthread_detach(pthread_self());

	str_echo(connfd);

	close(connfd);

	return NULL;	
}

int main(int argc,char * argv[])
{
	int listenfd = 0, connfd = 0;

	pthread_t tid;

	socklen_t addrlen, len;

	struct sockaddr * cliaddr;

	if ( argc == 2 )
	{
		listenfd = tcp_listen(NULL,argv[1],&addrlen);

	}

	else if ( argc == 3 )
	{
		listenfd = tcp_listen(argv[1],argv[2],&addrlen);
	}

	else
	{
		fprintf(stderr,"Error: tcpserv[ <host> ] <service or port> at %s in %d: %d\n",__FILE__,__LINE__);
		exit(1);
	}

	cliaddr = malloc(addrlen);

	for ( ; ; )
	{
		len = addrlen;

		int * iptr = malloc(sizeof(int));

		*iptr = accept(listenfd,cliaddr,&len);	

		pthread_create(&tid,NULL,&doit,(void*)iptr);
	}
}
