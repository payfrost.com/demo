Impossible To Securely Wipe RAM in Python:

https://stackoverflow.com/questions/728164/securely-erasing-password-in-memory-python

Python Threading:

https://docs.python.org/3/library/_thread.html

Preventing Duplicate Entries of SQLite3:

https://stackoverflow.com/questions/57685385/how-to-avoid-inserting-duplicate-data-when-inserting-data-into-sqlite3-database

Selecting For Specific Entries in SQLite3:

https://www.tutorialspoint.com/python_data_access/python_sqlite_select_data.htm

Validating Existence of Entry in SQLite3:

https://sqlite.org/faq.html

Handling Double Submit Cookies:

https://github.com/OWASP/CheatSheetSeries/blob/master/cheatsheets/Cross-Site_Request_Forgery_Prevention_Cheat_Sheet.md

https://security.stackexchange.com/questions/61110/why-does-double-submit-cookies-require-a-separate-cookie

https://stackoverflow.com/questions/3664044/anti-csrf-token-and-javascript


https://security.stackexchange.com/questions/149607/use-header-instead-of-cookie-for-csrf-double-submit-cookies

https://security.stackexchange.com/questions/220797/is-the-double-submit-cookie-pattern-still-effective

https://security.stackexchange.com/questions/248385/csrf-double-submit-cookie-pattern-questions


Most Important URLs on How To Use Cookies/Tokens Wisely:

***Bearer Token JWT Stored in Cookie and Later Placed in Transaction Header "Authorization: Bearer" sufficient

defense against CSRF (Warning: Does not by itself protect against XSS -- Set Separate Session Cookie as

HttpOnly):

***https://security.stackexchange.com/questions/170388/do-i-need-csrf-token-if-im-using-bearer-jwt

"Authorization: Bearer" header safe since it is not on the CORS-safe whitelist by default and

DO NOT put the "Authorization" header in the safelist (See above asterisked URL link)

HTTP Cookies in Browser (Mozilla):

https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies

The Web API Authentication Guide, The Intro:

****https://www.securitydrops.com/the-web-authentication-guide/

Cookies:

****https://www.securitydrops.com/cookies/

Defense Against XSS & CSRF in Browsers for Stateful Requests:

***** https://secgroup.dais.unive.it/wp-content/uploads/2020/02/csrf.pdf

OWASP Cookie Security:

https://owasp.org/www-chapter-london/assets/slides/OWASPLondon20171130_Cookie_Security_Myths_Misconceptions_David_Johansson.pdf

Safely Storing CSRF Tokens in LocalStorage:

https://stackoverflow.com/questions/35291573/csrf-protection-with-json-web-tokens

SOP Is Alone Insufficient to Totally Defend Against CSRF:

https://stackoverflow.com/questions/33261244/why-same-origin-policy-isnt-enough-to-prevent-csrf-attacks#33324803

Steal CSRF Token:

https://stackoverflow.com/questions/39536888/steal-csrf-token

Why Cookie-To-CSRF-Token is Acceptable:

https://typeofnan.dev/using-cookie-based-csrf-tokens-for-your-single-page-application/

Handling JWT in Browser:

https://javascript.plainenglish.io/how-to-secure-jwt-in-a-single-page-application-6a46e69fc393

https://medium.com/swlh/whats-the-secure-way-to-store-jwt-dd362f5b7914

https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie/SameSite

https://stackoverflow.com/questions/27067251/where-to-store-jwt-in-browser-how-to-protect-against-csrf

https://programarivm.com/how-to-store-jwt-and-oauth-access-tokens-as-per-owasp-guidelines

https://stackoverflow.com/questions/53668165/where-is-safest-to-store-json-web-tokens-jwts-in-client-side

https://www.jonathans-tech-blog.com/where-to-store-jwt-tokens/

https://spring.io/blog/2013/08/21/spring-security-3-2-0-rc1-highlights-csrf-protection/

Synchronizer Token (Anti-CSRF Token):

https://cheatsheetseries.owasp.org/cheatsheets/Cross-Site_Request_Forgery_Prevention_Cheat_Sheet.html#Synchronizer_.28CSRF.29_Tokens

Handling JWT in Python:

https://auth0.com/blog/how-to-handle-jwt-in-python/

Python Flask CORS Guide for Access-Control-Allow-Origin:

https://www.arundhaj.com/blog/definitive-guide-to-solve-cors-access-control-allow-origin-python-flask.html

Python Gunicorn:

https://martin-thoma.com/flask-gunicorn/

Python  .dotenv:

https://dev.to/jakewitcher/using-env-files-for-environment-variables-in-python-applications-55a1

Epoll in 3 Easy Steps:

https://www.suchprogramming.com/epoll-in-3-easy-steps/

The C10K Problem: http://www.kegel.com/c10k.html

High-Performance Server Architecture: http://pl.atyp.us/content/tech/servers.html
https://www.jmarshall.com/easy/http/

I/O Multiplexing and Non-Blocking: https://eklitzke.org/blocking-io-nonblocking-io-and-epoll

https://web.archive.org/web/20211024121356/https://xss.pwnfunction.com/

https://js.libhunt.com/libs/sanitize

https://linuxhint.com/free_xss_tools/

https://github.com/mozilla-spidermonkey/spidermonkey-embedding-examples

https://www.cs.princeton.edu/courses/archive/spr07/cos461/web_proxy.html

URL Sanitization: HTTP: The Definitive Guide: pgs 35-7

HTTP 1.1 Example in Golang:

https://baoqger.github.io/2021/10/25/understand-http1-1-persistent-connection-golang/

FreeBSD Notes:

https://unixsheikh.com/articles/technical-reasons-to-choose-freebsd-over-linux.html#security

Why WhatsApp Chose FreeBSD over Linux:

https://www.quora.com/What-is-FreeBSD-used-for?share=1

https://www.quora.com/Why-did-WhatsApp-choose-FreeBSD-over-Linux
