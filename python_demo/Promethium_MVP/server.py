from flask import Flask, request, make_response
from flask import render_template, abort, redirect, url_for
from flask import jsonify
from flask import session
from flask_cors import CORS, cross_origin
from utils import *

app = Flask(__name__)

#CORS(app,resources={r"/*": {"origins": "*"}})

CORS(app,resources={"/*": {"origins": ["https://promethiumex.com"]}})

@app.route("/",methods=['GET'])
def indexRoute():
	return render_template('index.html')

# get_link_token: This is where session cookie and Anti-CSRF Token is set

# 
@app.route("/get-link-token",methods=['GET'])
def get_link_token():
  
#  Convert HTTP Response headers to JSON

   json_resp = json.dumps(request.headers)

   if ('Cookie' in json_resp) and (verify_session(json_resp,'~/db/users.db') == True):
      return render_template('session_detected.html') , 303

#  First check if user already has a valid session cookie and Anti-CSRF Token

#  If that is the case, simply redirect them to their session

#  or notify them that they already have a valid session.

   db = sqlite3.connect('~/db/users.db')

   cur = db.cursor()

   cur.execute('''CREATE TABLE IF NOT EXISTS users(session_id_hmac BLOB PRIMARY KEY,anti_csrf_token_hmac BLOB,access_token_enc BLOB,item_id BLOB,request_id BLOB,link_session_id BLOB,deadline REAL,UNIQUE(session_id_hmac,anti_csrf_token_hmac,access_token_enc,item_id,request_id,link_session_id,deadline) ON CONFLICT IGNORE);''')
   
   db.commit()

   session_id = pysodium.randombytes(32)

# Convert session_id of type 'bytes' to Base64 'bytes' array

   session_id_base64_str = base64.b64encode(session_id.encode('utf-8')).decode()

# Convert session_id base64-encoded string of class 'bytes' to hmac of class 'str'

   session_id_hmac = hmac_message(raw_passwd,session_id_base64_str)

   anti_csrf_token = pysodium.randombytes(32)

# Convert session_id of type 'bytes' to Base64 'bytes' str

   anti_csrf_token_base64_str = base64.b64encode(session_id.encode('utf-8')).decode()

# Convert session_id base64-encoded string of class 'str' to hmac of class 'str'

   anti_csrf_token_hmac = hmac_message(raw_passwd,anti_csrf_token_base64_str)

#    Need to include deadline column to invalidate and then delete entry

   deadline = time.time() + 7200

   columns = (session_id_hmac,anti_csrf_token_hmac,access_token_enc,"","","",deadline)
   
   cur.execute('''INSERT INTO users(session_id_hmac,anti_csrf_token_hmac,access_token_enc,item_id,request_id,link_session_id,deadline) VALUES(?,?,?,?,?,?,?);''',columns)
   
   db.commit()

   db.close()

   cookie_body = "__Host-token=" + session_id_base64_str + " ; path=/ ; Secure ; SameSite=Lax ; HttpOnly ; Max-Age=7200"

   headers = {"Content-Type": "application/json","Set-Cookie": cookie_body, "CSRF-TOKEN": anti_csrf_token_base64_str}

   link_resp_json = getLinkToken()

#  User will successfully send back both Cookie + CSRF Token

#  if and only if user is making HTTP request from

#  promethiumex.com origin (Same Origin Policy)

   return link_resp_json['link_token'], 200, headers

@app.route("/get-sandbox-token",methods=['GET'])
def get_sandbox_token():
   
   json_resp = json.dumps(request.headers)

   if ('Cookie' in json_resp) and (verify_session(json_resp,'~/db/sandbox_users.db') == True):
      return render_template('session_detected.html') , 303

#  First check if user already has a valid session cookie and Anti-CSRF Token

#  If that is the case, simply redirect them to their session

#  or notify them that they already have a valid session.

   db = sqlite3.connect('~/db/sandbox_users.db')

   cur = db.cursor()

   cur.execute('''CREATE TABLE IF NOT EXISTS users(session_id_hmac BLOB PRIMARY KEY,anti_csrf_token_hmac BLOB,access_token_enc BLOB,item_id BLOB,request_id BLOB,link_session_id BLOB,deadline REAL,UNIQUE(session_id_hmac,anti_csrf_token_hmac,access_token_enc,item_id,request_id,link_session_id,deadline) ON CONFLICT IGNORE);''')
   
   db.commit()

   session_id = pysodium.randombytes(32)

# Convert session_id of type 'bytes' to Base64 'bytes' array

   session_id_base64_str = base64.b64encode(session_id.encode('utf-8')).decode()

# Convert session_id base64-encoded string of class 'bytes' to hmac of class 'str'

   session_id_hmac = hmac_message(raw_passwd,session_id_base64_str)

   anti_csrf_token = pysodium.randombytes(32)

# Convert session_id of type 'bytes' to Base64 'bytes' str

   anti_csrf_token_base64_str = base64.b64encode(session_id.encode('utf-8')).decode()

# Convert session_id base64-encoded string of class 'str' to hmac of class 'str'

   anti_csrf_token_hmac = hmac_message(raw_passwd,anti_csrf_token_base64_str)

#    Need to include deadline column to invalidate and then delete entry

   deadline = time.time() + 7200

   columns = (session_id_hmac,anti_csrf_token_hmac,access_token_enc,"","","",deadline)
   
   cur.execute('''INSERT INTO users(session_id_hmac,anti_csrf_token_hmac,access_token_enc,item_id,request_id,link_session_id,deadline) VALUES(?,?,?,?,?,?,?);''',columns)
   
   db.commit()

   db.close()

   cookie_body = "__Host-token=" + session_id_base64_str + " ; path=/ ; Secure ; SameSite=Lax ; HttpOnly ; Max-Age=7200"

   headers = {"Content-Type": "application/json","Set-Cookie": cookie_body, "CSRF-TOKEN": anti_csrf_token_base64_str}

   sandbox_resp_json = getSandboxToken()

#  User will successfully send back both Cookie + CSRF Token

#  if and only if user is making HTTP request from

#  promethiumex.com origin (Same Origin Policy)

   return sandbox_resp_json['public_token'], 200, headers


@app.route("/link-integration",methods=['GET'])
def link_integration():
    return render_template('link.html')

@app.route("/sandbox-integration",methods=['GET'])
def sandbox_integration():
    return render_template('sandbox.html')

# Need to leave session_id and CSRF Token secret in HMAC state in database

# Need to parse session_id and CSRF Token in store_access_token

# Need to UPDATE table entry for user, not INSERT the first time. That first time happened in get-link-token

@app.route("/store-access-token",methods=['POST'])
def store_access_token():
   
   json_resp = json.dumps(request.headers)

   if ('Cookie' in json_resp) and (verify_session(json_resp,'~/db/users.db') == True):
      return render_template('session_detected.html') , 303

   resp = request.get_json()
   
   public_token = resp['public_token']

   link_session_id = resp['link_session_id']

#  Exchange public token for access token here

   access_token_enc = fetch_development_access_token(public_token)

   db = sqlite3.connect('~/db/users.db')

   cur = db.cursor()

   sql_statement = "UPDATE users SET access_token_enc=\'" 

   sql_statement = sql_statement + access_token_enc + "\' WHERE session_id_hmac=\'" 

   sql_statement = sql_statement + session_id_hmac + "\'" + ";"

   cur.execute(sql_statement)
   
   db.commit()
   
   sql_statement = "UPDATE users SET link_session_id=\'" 

   sql_statement = sql_statement + link_session_id + "\' WHERE session_id_hmac=\'" 

   sql_statement = sql_statement + session_id_hmac + "\'" + ";"

   cur.execute(sql_statement)
   
   db.commit()

   db.close()

@app.route("/store-sandbox-access-token",methods=['POST'])
def store_sandbox_access_token():
   
   json_resp = json.dumps(request.headers)

   if ('Cookie' in json_resp) and (verify_session(json_resp,'~/db/users.db') == True):
      return render_template('session_detected.html') , 303

   resp = request.get_json()
   
   public_token = resp['public_token']

#  Exchange public token for access token here

   access_token_enc = fetch_development_access_token(public_token)

   db = sqlite3.connect('~/db/users.db')

   cur = db.cursor()

   sql_statement = "UPDATE users SET access_token_enc=\'" 

   sql_statement = sql_statement + access_token_enc + "\' WHERE session_id_hmac=\'" 

   sql_statement = sql_statement + session_id_hmac + "\'" + ";"

   cur.execute(sql_statement)
   
   db.commit()

   db.close()


@app.route("/demo",methods=['GET'])
def loginRoute():
	print(searchInstutions("Bank of America"))
	return render_template('login.html')

@app.route("/gpg",methods=['GET'])
def gpg():

    headers = {"Content-Type":"text/plain ; charset=utf-8"}

    return render_template('gpg.asc'), 200, headers

if __name__ == "__main__":
    app.run()
