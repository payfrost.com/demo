import pysodium
import binascii
from getpass import getpass

salt = binascii.unhexlify(b'00000000000000000000000000000000')

raw_passwd = getpass()

key = pysodium.crypto_pwhash(pysodium.crypto_auth_KEYBYTES,raw_passwd,salt,pysodium.crypto_pwhash_argon2id_OPSLIMIT_INTERACTIVE,pysodium.crypto_pwhash_argon2id_MEMLIMIT_INTERACTIVE,pysodium.crypto_pwhash_ALG_ARGON2ID13)

ad = binascii.unhexlify(b'00000000000000000000000000000000')

nonce = binascii.unhexlify(b"000000000000000000000000000000000000000000000000")

raw_msg = getpass()

secret_raw = bytes.fromhex(raw_msg)

'''
1. Convert secret_hexstring from str to hex-encoded 'bytes' object

2. Convert this 'bytes' object back to a raw 'bytes' array
'''

z = pysodium.crypto_aead_xchacha20poly1305_ietf_decrypt(secret_raw,ad,nonce,key)

'''
1. In variable z, the secret development key is a 'bytes' array. You must convert it

to string form.
'''

zstr = z.decode("utf-8")
