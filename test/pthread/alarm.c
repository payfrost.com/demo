#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc,char*argv[])
{
	int seconds = 0;

	unsigned char line[129];

	unsigned char msg[65];

	while (1)
	{
		printf("Alarm> ");

		if ( fgets(line,sizeof(line),stdin) == NULL )
		{
			exit(0);
		}

		if (strnlen(line,128) <= 1 ) continue;

		if ( sscanf(line,"%d %64[^\n]",&seconds,msg) < 2 )
		{
			fprintf(stderr,"Bad command\n");
		}

		else
		{
			sleep(seconds);

			printf("(%d) %s\n",seconds,msg);
		}
	}
}
