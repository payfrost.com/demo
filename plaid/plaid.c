#ifndef __PSEC_H__
#define __PSEC_H__
#include "../headers/psec.h"
#endif

void createPublicToken(unsigned char * json_resp,const size_t json_resp_size)
{
	const char method[] = "POST\0";

	const char url[] = "https://sandbox.plaid.com/sandbox/public_token/create\0";

	const char file_path[] = "/tmp/sandbox_public_token.json\0";

	const char * headers[] =

	{

		"Content-Type: application/json"

		,

		NULL
		
	};

	const char * payload = NULL;

	unsigned char * resp = NULL;

	const size_t num_headers = 1, response_size = 1024;

//	http_request(const unsigned char * method,const unsigned char * url,const unsigned char ** headers,const size_t num_headers,const unsigned char * payload,const unsigned char * file_path,unsigned char ** response,const size_t response_size);

	http_request(method,url,headers,num_headers,payload,file_path,&resp,response_size);

	memset_s(json_resp,json_resp_size,0x00,json_resp_size);

	FILE * in = NULL;

	if ( ( in = fopen("/tmp/sandbox_public_token.json","r+") ) == NULL )
	{
		fprintf(stderr,"Error: Failed to open file.\nAborting\n");

		exit(0);
	}

	size_t file_size = 0;

	if ( fseek(in,0L,SEEK_END) != 0 )
	{
		fprintf(stderr,"Error: Failed to seek end of file.\nAborting\n");

		exit(0);
	}


	file_size = ftell(in);
	
	rewind(in);

	file_to_json_string(json_resp,file_size,in);

	fclose(in);
	
	unsigned char * search_result = NULL;

//	printf("x:%s\n",json_resp);

	search_result = search(json_resp,"\"secret\":");

	size_t i = 0;

	for (   i = (search_result - &json_resp[0]); json_resp[i] != ','; i++ )
		;

	i++;
	

	if ( (search_result = search(resp,(const char*)"\"public_token\0") ) == NULL )
	{
		fprintf(stderr,"Error: HTTP POST request to create Plaid Token failed.\nAborting.\n");

		exit(0);
	}

	size_t j = search_result - resp;

	for ( ; resp[j] != ','; i++, j++ )
	{
		json_resp[i] = resp[j];	
	}

	json_resp[i] = '}';

	i++;

	json_resp[i] = 0x00;

	
//	printf("resp:%s\n",resp);
	
//	printf("x:%s\n",json_resp);

#if 0
	unsigned char * srp = search_result;

	while ( srp != '"' )
	{
		
	}
#endif
	
	free(resp);

}


void exchangePublicForAccessToken(const unsigned char * json_request,const size_t json_request_size,unsigned char ** resp,const size_t resp_size)
{
	const char method[] = "POST\0";

	const char url[] = "https://sandbox.plaid.com/item/public_token/exchange\0";

	const char * file_path = NULL;

	const char * headers[] =

	{

		"Content-Type: application/json"

		,

		NULL
		
	};

	const char * payload = json_request;

	const size_t num_headers = 1; 

//	http_request(const unsigned char * method,const unsigned char * url,const unsigned char ** headers,const size_t num_headers,const unsigned char * payload,const unsigned char * file_path,unsigned char ** response,const size_t response_size);

	http_request(method,url,headers,num_headers,payload,file_path,resp,resp_size);
	
}

bool getAccountBalanceIfValid(unsigned char customerBankAccount[]) {
#if 0
	CURL *curl = curl_easy_init();
	
	CURLcode res;

	string accessToken = exchangePublicForAccessToken()

	// string sJson = 'client_id=618bcf866c40c40013e5df50&secret=03e3e34db4951d3b5ce31303f5fa22&access_token=XX'

	memset_s((unsigned char*)&res,sizeof(CURLcode),0x00, sizeof(CURLcode));

	if (curl != NULL) {
		curl_easy_setopt(curl, CURLOPT_URL, "https://sandbox.plaid.com/accounts/balance/get");

		struct curl_slist* slist = NULL;
		slist = curl_slist_append(slist, "Content-Type: application/json");
		slist = curl_slist_append(slist, "client_id: 618bcf866c40c40013e5df50");
		slist = curl_slist_append(slist, "secret: 03e3e34db4951d3b5ce31303f5fa22");

		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);

		// curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, response_callBack);
		
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "access_token=" + accessToken);
		curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, 12);
		
		res = curl_easy_perform(curl);
		
		if ( res != CURLE_OK ) {
			fprintf(stderr,"curl_easy_perform() failed: %s\n",curl_easy_strerror(res));
			return false;
		}
	}

	curl_easy_cleanup(curl);
	return true;
#endif
	return false;
}

bool parseBankAccount(unsigned char customerBankAccount[], const size_t n) {

    for(size_t i = 0; i < strnlen_s(customerBankAccount, n); i++) {
        if( !isdigit(customerBankAccount[i]))  return false;
    }
    
    return true;
}
