package main

import (
//	"bytes"
//	"crypto/rand"
//	"crypto/tls"
//	"crypto/x509"
//	"math/big"
//	"io/ioutil"
//	"os"
	"log"
	"net/http"
//	"fmt"
//	"strconv"
//	"strings"
	"sync"
//	"golang.org/x/crypto/chacha20poly1305"
)


func stress(wg * sync.WaitGroup)		{

	if wg != nil	{
		
		defer wg.Done()
	}

	resp, err := http.Get("http://localhost:9877")

	if err != nil	{
		
		log.Fatalln(err)
	}

	resp.Body.Close()

}

func main()	{

	wg := new(sync.WaitGroup)

	var i uint64 = 1

	for ; ; 			{

		for ; i <= 300 ; i++	{

			wg.Add(1)

			go stress(wg)
		}

		wg.Wait()

	}

}
