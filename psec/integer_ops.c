#ifndef __PSEC_H__
#define __PSEC_H__
#include "../headers/psec.h"
#endif

/*
#ifndef __LIMITS_H__
#define __LIMITS_H__
#include <limits.h>
#endif

#ifndef ULLONG_MAX
#define ULLONG_MAX	18446744073709551615
#endif

#ifndef __STDIO_H__
#define __STDIO_H__
#include <stdint.h>
#endif

#ifndef __STDIO_H__
#define __STDIO_H__
#include <stdio.h>
#endif

#ifndef __STDARG_H__
#define __STARG_H__
#include <stdlib.h>
#endif

#ifndef __STDARG_H__
#define __STARG_H__
#include <stdarg.h>
#endif
*/

/* CERT C compliant addition */

unsigned char uchar_add(size_t n,...);

unsigned short ushort_add(size_t n,...);

unsigned int uint_add(size_t n,...);

unsigned long int ulong_add(size_t n,...);

unsigned long long int ulong_long_add(size_t n,...);

/* CERT C compliant subtraction */

unsigned char uchar_sub(size_t n,...);

unsigned short ushort_sub(size_t n,...);

unsigned int uint_sub(size_t n,...);

unsigned long int ulong_sub(size_t n,...);

unsigned long long int ulong_long_sub(size_t n,...);


/* CERT C compliant multiplication */

unsigned char uchar_mult(size_t n,...);

unsigned short ushort_mult(size_t n,...);

unsigned int uint_mult(size_t n,...);

unsigned long int ulong_mult(size_t n,...);

unsigned long long int ulong_long_mult(size_t n,...);

unsigned char uchar_add(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned char sum = 0;

	unsigned char sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned char);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( ( sum + sum_i ) < sum )
		{
			fprintf(stderr,"Error Code %hhu: Integer Overflow detected: %hhu + %hhu > %hhu\n",uchar_overflow,sum,sum_i,UCHAR_MAX);				

			exit(0);
		}

		else
		{
			sum = sum + sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;
}


unsigned short ushort_add(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned short sum = 0;

	unsigned short sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned short);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( ( sum + sum_i ) < sum )
		{
			fprintf(stderr,"Error Code %hu: Integer Overflow detected: %hu + %hu > %hu\n",ushort_overflow,sum,sum_i,USHRT_MAX);				
			
			exit(0);
		
		}
		
		else
		{
			sum = sum + sum_i;
		}
		
		i++;
	}

	va_end(var_arg);

	return sum;
}


unsigned int uint_add(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned int sum = 0;

	unsigned int sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned int);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( ( sum + sum_i ) < sum )
		{
			fprintf(stderr,"Error Code %u: Integer Overflow detected: %u + %u > %u\n",uint_overflow,sum,sum_i,UINT_MAX);				
		
			exit(0);
		}
		
		else
		{
			sum = sum + sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;

}


unsigned long int ulong_add(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned long int sum = 0;

	unsigned long int sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned long int);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( ( sum + sum_i ) < sum )
		{
			fprintf(stderr,"Error Code %lu: Integer Overflow detected: %lu + %lu > %lu\n",ulong_overflow,sum,sum_i,ULONG_MAX);				
		
			exit(0);
		}
		
		else
		{
			sum = sum + sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;

}


unsigned long long int ulong_long_add(size_t n,...)
{
	
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned long long int sum = 0;

	unsigned long long int sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned long long int);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( ( sum + sum_i ) < sum )
		{
			fprintf(stderr,"Error Code %llu: Integer Overflow detected: %llu + %llu > %llu\n",ulong_long_overflow,sum,sum_i,ULLONG_MAX);				
			
			exit(0);
		
		}
		
		else
		{
			sum = sum + sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;

}

unsigned char uchar_sub(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned char sum = 0;

	unsigned char sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned char);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( ( sum - sum_i ) > sum )
		{
			fprintf(stderr,"Error Code %hhu: Integer Underflow detected: %hhu - %hhu < %hhu\n",uchar_underflow,sum,sum_i,sum);				

			exit(0);
		}

		else
		{
			sum = sum - sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;

}

unsigned short ushort_sub(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned short sum = 0;

	unsigned short sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned short);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( ( sum - sum_i ) > sum )
		{
			fprintf(stderr,"Error Code %hhu: Integer Underflow detected: %hu - %hu < %hu\n",ushort_underflow,sum,sum_i,sum);				

			exit(0);
		}

		else
		{
			sum = sum - sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;

}


unsigned int uint_sub(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned int sum = 0;

	unsigned int sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned short);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( ( sum - sum_i ) > sum )
		{
			fprintf(stderr,"Error Code %hhu: Integer Underflow detected: %u - %u < %u\n",uint_underflow,sum,sum_i,sum);				

			exit(0);
		}

		else
		{
			sum = sum - sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;

}

unsigned long int ulong_sub(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned long int sum = 0;

	unsigned long int sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned long int);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( ( sum - sum_i ) > sum )
		{
			fprintf(stderr,"Error Code %hhu: Integer Underflow detected: %lu - %lu < %lu\n",ulong_underflow,sum,sum_i,sum);				

			exit(0);
		}

		else
		{
			sum = sum - sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;

}

unsigned long long int ulong_long_sub(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned long int sum = 0;

	unsigned long int sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned long int);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( ( sum - sum_i ) > sum )
		{
			fprintf(stderr,"Error Code %hhu: Integer Underflow detected: %llu - %llu < %llu\n",ulong_long_underflow,sum,sum_i,sum);				

			exit(0);
		}

		else
		{
			sum = sum - sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;

}

/* CERT C compliant multiplication */

unsigned char uchar_mult(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned char sum = 0;

	unsigned char sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned char);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( ( sum * sum_i ) < sum )
		{
			fprintf(stderr,"Error Code %hhu: Integer Overflow detected: %hhu * %hhu > %hhu\n",uchar_overflow,sum,sum_i,UCHAR_MAX);				

			exit(0);
		}

		else
		{
			sum = sum * sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;
}

unsigned short ushort_mult(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned short sum = 0;

	unsigned short sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned short);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( ( sum * sum_i ) < sum )
		{
			fprintf(stderr,"Error Code %hu: Integer Overflow detected: %hu * %hu > %hu\n",ushort_overflow,sum,sum_i,USHRT_MAX);				
			
			exit(0);
		
		}
		
		else
		{
			sum = sum * sum_i;
		}
		
		i++;
	}

	va_end(var_arg);

	return sum;
}

unsigned int uint_mult(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned int sum = 0;

	unsigned int sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned int);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( ( sum * sum_i ) < sum )
		{
			fprintf(stderr,"Error Code %u: Integer Overflow detected: %u * %u > %u\n",uint_overflow,sum,sum_i,UINT_MAX);				
		
			exit(0);
		}
		
		else
		{
			sum = sum * sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;

}

unsigned long int ulong_mult(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned long int sum = 0;

	unsigned long int sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned long int);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( ( sum * sum_i ) < sum )
		{
			fprintf(stderr,"Error Code %lu: Integer Overflow detected: %lu * %lu > %lu\n",ulong_overflow,sum,sum_i,ULONG_MAX);				
		
			exit(0);
		}
		
		else
		{
			sum = sum * sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;

}

unsigned long long int ulong_long_mult(size_t n,...)
{
	
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned long long int sum = 0;

	unsigned long long int sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned long long int);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( ( sum * sum_i ) < sum )
		{
			fprintf(stderr,"Error Code %llu: Integer Overflow detected: %llu * %llu > %llu\n",ulong_long_overflow,sum,sum_i,ULLONG_MAX);				
			
			exit(0);
		
		}
		
		else
		{
			sum = sum * sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;

}

/* CERT C compliant division */

unsigned char uchar_div(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned char sum = 0;

	unsigned char sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned char);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( sum_i == 0 )
		{
			fprintf(stderr,"Error Code %hhu: Divide-By-Zero-Error detected: %hhu / %hhu",uchar_div_by_zero,sum,sum_i);				

			exit(0);
		}

		else
		{
			sum = sum / sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;
}

unsigned short ushort_div(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned short sum = 0;

	unsigned short sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned char);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( sum_i == 0 )
		{
			fprintf(stderr,"Error Code %hhu: Divide-By-Zero-Error detected: %hu / %hu",ushort_div_by_zero,sum,sum_i);				

			exit(0);
		}

		else
		{
			sum = sum / sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;
}

unsigned int uint_div(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned int sum = 0;

	unsigned int sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned int);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( sum_i == 0 )
		{
			fprintf(stderr,"Error Code %hhu: Divide-By-Zero-Error detected: %hhu / %hhu",uint_div_by_zero,sum,sum_i);				

			exit(0);
		}

		else
		{
			sum = sum / sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;
}

unsigned long int ulong_div(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned long int sum = 0;

	unsigned long int sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned long int);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( sum_i == 0 )
		{
			fprintf(stderr,"Error Code %hhu: Divide-By-Zero-Error detected: %lu / %lu",ulong_div_by_zero,sum,sum_i);				

			exit(0);
		}

		else
		{
			sum = sum / sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;
}

unsigned long long int ulong_long_div(size_t n,...)
{
	va_list var_arg;

	va_start(var_arg,n);
	
	unsigned long long int sum = 0;

	unsigned long long int sum_i = 0;

	size_t i = 0;

	while ( i < n )
	{
		sum_i = va_arg(var_arg,unsigned long long int);

		if ( sum == 0 )
		{
			sum = sum_i;
		}

		else if ( sum_i == 0 )
		{
			fprintf(stderr,"Error Code %hhu: Divide-By-Zero-Error detected: %llu / %llu",ulong_long_div_by_zero,sum,sum_i);				

			exit(0);
		}

		else
		{
			sum = sum / sum_i;
		}

		i++;
	}

	va_end(var_arg);

	return sum;
}

