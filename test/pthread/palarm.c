#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

typedef struct alarm_t
{
	int seconds;

	unsigned char message[65];

} alarm_t;

void * alarm_thread(void*arg)
{
	alarm_t * alarm = (alarm_t*)arg;

	int status = 0;

	status = pthread_detach(pthread_self());

	if ( status != 0 )
	{
		fprintf(stderr,"Error: Failed to detach thread.\nAborting.\n");

		exit(1);
	}

	sleep(alarm->seconds);

	printf("(%d) %s\n",alarm->seconds,alarm->message);

	free(alarm);

	return NULL;
}

int main(int argc,char*argv[])
{
	int status = 0;

	unsigned char line[129];

	alarm_t * alarm;

	pthread_t thread;

	while ( 1 )
	{
		memset(line,0x00,129*sizeof(unsigned char));

		printf("Alarm>");

		if ( fgets(line,sizeof(line),stdin) == NULL )
		{
			fprintf(stderr,"Error: Failed to get a line.\n");

			exit(1);
		}

		if ( strnlen(line,128) <= 1 ) continue;

		alarm = (alarm_t*)calloc(1,sizeof(alarm_t));

		if ( alarm == NULL )
		{
			fprintf(stderr,"Error: Failed to allocate alarm\n");

			exit(1);
		}
		
		if ( sscanf(line,"%d %64[^\n]",&alarm->seconds,alarm->message) < 2 )
		{
			fprintf(stderr,"Bad command\n");

			free(alarm);
		}

		else
		{
			printf("Made it.\n");

			status = pthread_create(&thread,NULL,alarm_thread,alarm);

			if ( status != 0 )
			{
				fprintf(stderr,"Error: Failed to spawn thread for alarm_thread().\nAborting.\n");
				exit(1);
			}
		}

	}
	
	return 0;
}
