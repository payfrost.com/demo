print("Debug", getLinkToken() )

print(":: Welcome to PromethiumEX ::")
print("Please enter a valid Bank Account: ")
bankAccount = input()

while True:
	if len(bankAccount) < 10:
		print("You entered less than 10 digits. Please try again! ")
		bankAccount = input()
	elif len(bankAccount) > 12:
		print("You entered more than 12 digits. Please try again! ")
		bankAccount = input()
	elif validBankAccount(bankAccount):
		break
	else:
		print("Please enter a valid Bank Account: ")
		bankAccount = input()

print("Bank Account Number: ", bankAccount)
print("Enter 1 to check account summary and others to check available balance")

generateAccessToken()

response = input()

if response == "1":
	print("Please wait while we process the request! ")
	print("Account Summary: ", getAccountSummary() )
else:
	print("You current have: ", getBankBalance() )
	print("Do you wish to transfer fund to another account? ")