#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <arpa/inet.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/un.h>

int max(int a,int b)
{
	return a > b ? a : b;
}
void str_cli(FILE * fp,int sockfd)
{
	int maxfdp1 = 0, stdineof = 0;

	fd_set rset;

	unsigned char buf[1025];

	int n = 0;

	stdineof = 0;

	FD_ZERO(&rset);

	for ( ; ; )
	{
		if ( stdineof == 0 )
		{
			FD_SET(fileno(fp),&rset);
		}

		FD_SET(sockfd,&rset);

		maxfdp1 = max(fileno(fp),sockfd) + 1;

		int select_status = select(maxfdp1,&rset,NULL,NULL,NULL);

		if ( select_status < 0 )
		{
			fprintf(stderr,"Error: Failed to set select_status.\nAborting.\n");

			exit(1);
		}

		if ( FD_ISSET(sockfd,&rset) )
		{
			if ( ( n = read(sockfd,buf,1024) ) == 0 )
			{
				if ( stdineof == 1 )
				{
					return;
				}

				else
				{
					fprintf(stderr,"Error: Server terminated prematurely.\n");

					exit(1);
				}
			}

			write(fileno(stdout),buf,n);
		}

		if ( FD_ISSET(fileno(fp),&rset) )
		{
			if ( ( n = read(fileno(fp),buf,1024) ) == 0 )
			{
				stdineof = 1;

				shutdown(sockfd,SHUT_WR);

				FD_CLR(fileno(fp),&rset);

				continue;
			}	
		
			write(sockfd,buf,n);	
		}
	}

}



int main(int argc,char*argv[])
{
	int i = 0, sockfd[5];

	struct sockaddr_in servaddr;

	if ( argc != 2 )
	{
		fprintf(stderr,"Error: Required one CLI argument.\nAborting.\n");

		exit(1);
	}

	for (i = 0; i < 5 ; i++ )
	{
		sockfd[i] = socket(AF_INET,SOCK_STREAM,0);

		memset((unsigned char*)&servaddr,0x00,sizeof(servaddr));

		servaddr.sin_family = AF_INET;

		servaddr.sin_port = htons(9877);

		inet_pton(AF_INET,argv[1],&servaddr.sin_addr);

		connect(sockfd[i],(struct sockaddr*)&servaddr,sizeof(servaddr));

	}
	
	str_cli(stdin,sockfd[0]);

	exit(0);

	return 0;
}
