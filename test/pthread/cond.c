#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <time.h>

typedef struct my_struct_tag
{
	pthread_mutex_t mutex;

	pthread_cond_t cond;

	int value;

} my_struct_t;

my_struct_t data = { PTHREAD_MUTEX_INITIALIZER, PTHREAD_COND_INITIALIZER, 0 };

int hibernation = 1;

void * wait_thread(void * arg)
{
	int status;

	sleep(hibernation);

	status = pthread_mutex_lock(&data.mutex);

	if ( status != 0 )
	{
		fprintf(stderr,"Error: Failed to lock mutex at %s in line %d: %s\n",__FILE__,__LINE__,status);
		
		exit(1);
	}

	data.value = 1;

	status = pthread_cond_signal(&data.cond);

	if ( status != 0 )
	{
		fprintf(stderr,"Error: Failed to signal condition at %s in line %d: %s\n",__FILE__,__LINE__,status);
		exit(1);
	}

	status = pthread_mutex_unlock(&data.mutex);

	if ( status != 0 )
	{
		fprintf(stderr,"Error: Failed to unlock mutex at %s in line %d: %s\n",__FILE__,__LINE__,status);
		exit(1);
	}

	return NULL;
}

int main(int argc,char*argv[])
{
	int status = 0;

	pthread_t wait_thread_id;

	struct timespec timeout;

	if ( argc > 1 )
	{
		hibernation = atoi(argv[1]);
	}

	status = pthread_create(&wait_thread_id,NULL,wait_thread,NULL);

	if ( status != 0 )
	{
		fprintf(stderr,"Error: Failed to create wait thread at %s in %d: %d\n",__FILE__,__LINE__,status);
		exit(1);
	}

	timeout.tv_sec = time(NULL) + 2;

	timeout.tv_nsec = 0;

	status = pthread_mutex_lock(&data.mutex);

	if ( status != 0 )
	{
		fprintf(stderr,"Error: Failed to lock mutex at %s in %d: %d\n",__FILE__,__LINE__,status);

		exit(1);
	}


	while ( data.value == 0 )
	{
		status = pthread_cond_timedwait(&data.cond,&data.mutex,&timeout);

		if ( status == ETIMEDOUT )
		{
			printf("Condition wait timed out.\n");

			break;
		}

		else if ( status != 0 )
		{
			fprintf(stderr,"Error: Failed to wait on condition at %s in %d: %d\n",__FILE__,__LINE__,status);
			exit(1);
		}	
	}

	if ( data.value != 0 )
	{
		printf("Condition was signaled.\n");
	}

	status = pthread_mutex_unlock(&data.mutex);

	if ( status != 0 )
	{
		fprintf(stderr,"Error: Failed to unlock mutex at %s in %d: %d\n",__FILE__,__LINE__,status);

		exit(1);
	}

	return 0;
}
