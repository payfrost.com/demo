#ifndef __JEMALLOC_H__
#define __JEMALLOC_H__
#include <jemalloc/jemalloc.h>
#endif


#ifndef __LIMITS_H__
#define __LIMITS_H__
#include <limits.h>
#endif

#ifndef __MATH_H__
#define __MATH_H__
#include <math.h>
#endif

#ifndef __CTYPE_H__
#define __CTYPE_H__
#include <ctype.h>
#endif

#ifndef __SODIUM_H__
#define __SODIUM_H__
#include <sodium.h>
#endif

#ifndef __STDBOOL_H__
#define __STDBOOL_H__
#include <stdbool.h>
#endif

#ifndef __STDDEF_H__
#define __STDDEF_H__
#include <stddef.h>
#endif

#ifndef __STDINT_H__
#define __STDINT_H__
#include <stdint.h>
#endif

#ifndef __STDIO_H__
#define __STDIO_H__
#include <stdio.h>
#endif

#ifndef __STDARG_H__
#define __STARG_H__
#include <stdarg.h>
#endif

#ifndef __STDARG_H__
#define __STARG_H__
#include <stdlib.h>
#endif

#ifndef __STRING_H__
#define __STRING_H__
#include <string.h>
#endif

#if 0
#ifndef __IOSTREAM_H__
#define __IOSTREAM_H__
#include <iostream>
#endif

#ifndef __ALGORITHM_H__
#define __ALGORITHM_H__
#include <algorithm>
#endif

#ifndef __CSTRING_H__
#define __CSTRING_H__
#include <cstring>
#endif
#endif

#ifndef __CURL_H__
#define __CURL_H__
#include <curl/curl.h>
#endif

#ifndef ULLONG_MAX
#define ULLONG_MAX	18446744073709551615
#endif

#ifndef __GETPASS_H__
#define __GETPASS_H__
#include <termios.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <unistd.h>
#endif

#if 0
#ifndef __USING__
#define __USING__
using std::cout;
using std::endl;
using std::cin;
using std::string;
#endif
#endif

#ifndef __ENUM_H__
#define __ENUM_H__
enum err_code
{
	uchar_overflow=1,

	uchar_underflow=2,

	ushort_overflow=3,

	ushort_underflow=4,

	uint_overflow=5,

	uint_underflow=6,

	ulong_overflow=7,

	ulong_underflow=8,

	ulong_long_overflow=9,

	ulong_long_underflow=10,

	uchar_div_by_zero=11,

	ushort_div_by_zero=12,

	uint_div_by_zero=13,

	ulong_div_by_zero=14,

	ulong_long_div_by_zero=15

};
#endif

/* CERT C compliant addition */

size_t memset_s(unsigned char * dest,size_t dest_size,unsigned char ch,size_t count);

unsigned char uchar_add(size_t n,...);

unsigned short ushort_add(size_t n,...);

unsigned int uint_add(size_t n,...);

unsigned long int ulong_add(size_t n,...);

unsigned long long int ulong_long_add(size_t n,...);

/* CERT C compliant subtraction */

unsigned char uchar_sub(size_t n,...);

unsigned short ushort_sub(size_t n,...);

unsigned int uint_sub(size_t n,...);

unsigned long int ulong_sub(size_t n,...);

unsigned long long int ulong_long_sub(size_t n,...);


/* CERT C compliant multiplication */

unsigned char uchar_mult(size_t n,...);

unsigned short ushort_mult(size_t n,...);

unsigned int uint_mult(size_t n,...);

unsigned long int ulong_mult(size_t n,...);

unsigned long long int ulong_long_mult(size_t n,...);

/* cert c compliant division */

unsigned char uchar_div(size_t n,...);

unsigned short ushort_div(size_t n,...);

unsigned int uint_div(size_t n,...);

unsigned long int ulong_div(size_t n,...);

unsigned long long int ulong_long_div(size_t n,...);

// Boyer Moore String Pattern Matching

#ifndef __BADCHAR_ARR_SIZE
#define __BADCHAR_ARR_SIZE
#define NO_OF_CHARS 256
#endif

size_t max (size_t a, size_t b);

void badCharHeuristic( unsigned char *str, int size,int badchar[NO_OF_CHARS]);

unsigned char * search(unsigned char *txt,unsigned char *pat);


// CERT C Compliant String Functions

size_t strnlen_s(unsigned char *s,const size_t n);

size_t strncat_s(unsigned char * dst,const size_t dst_size,const unsigned char * src,size_t src_size,size_t n);

unsigned char * strnstr_s(unsigned char * str,const size_t str_size,unsigned char * substr);

// Functions handling JSON payload and response data

size_t file_to_json_string(unsigned char * json_string,size_t json_string_size,FILE * in);

// Sandbox Plaid Integration Functions

bool getAccountBalanceIfValid(unsigned char customerBankAccount[]);

bool parseBankAccount(unsigned char customerBankAccount[], const size_t n);

//void createPublicToken(unsigned char * json_resp,const size_t json_resp_size);

void createPublicToken(unsigned char * json_resp,const size_t json_resp_size);


void exchangePublicForAccessToken(const unsigned char * json_request,const size_t json_request_size,unsigned char ** resp,const size_t resp_size);

// Development Plaid Integration Functions

// void link_token_create(unsigned char * response,size_t response_size);

void link_token_create(unsigned char ** response,size_t * response_size,unsigned char * client_secret,size_t client_secret_size);


// Curl HTTP Request Functions
size_t write_callback(unsigned char * raw_download_contents,size_t size,size_t nmemb,unsigned char * download_contents_save);

void http_request(const unsigned char * method,const unsigned char * url,const unsigned char * headers[],const size_t num_headers,const unsigned char * payload,const unsigned char * file_path,unsigned char ** response,const size_t response_size);

// Base64 Encoding and Decoding Functions

void fill_base64_table(void);

void fill_base64_decode_table(void);

void base64_encode(unsigned char * input,const unsigned long long size,unsigned char *out);

void base64_decode(unsigned char * input,unsigned long long int size,unsigned char * out);

// Secure Function To Read Passwords

int get_pass(unsigned char * pwd,unsigned char * repwd,const size_t MAX_SIZE, FILE * stream);

// Encrypt and Decrypt Sensitive Strings To and From Files

void enc_string_to_file(const unsigned char * filename,size_t n);

void dec_file_to_string(const unsigned char * filepath,const size_t decrypt_string_size,unsigned char ** out,size_t * outsize);


