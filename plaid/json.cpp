#ifndef __PSEC_H__
#define __PSEC_H__
#include "../headers/psec.h"
#endif

size_t file_to_json_string(unsigned char json_string[],const size_t json_string_size,FILE * in)
{


	if ( in == NULL )
	{
		fprintf(stderr,"Error: Failed to open json file in file_to_json_string(). Aborting\n");

		return 0;
	}

	rewind(in);

	size_t file_size = 0;

	if ( fseek(in,0L,SEEK_END) != 0 )
	{
		fprintf(stderr,"Error: Failed to manually verify end of file in file_to_json_string(). Aborting\n");
		rewind(in);

		return 0;	
	}

	file_size = ftell(in);

	if ( file_size > json_string_size )
	{
		fprintf(stderr,"Error: Failed to allocate sufficient memory in json_string in \nfile_to_json_string(). Aborting.\n");
	
		return 0;
	}	

	rewind(in);

	size_t i = 0;

	unsigned char c = 0;

	for ( ; (i < file_size) && ( ( c = fgetc(in) ) != 0xff ); )
	{
		if ( !isspace(c) )
		{
			json_string[i] = c;	

			i++;
		}

	}

	rewind(in);

	return i;
}
